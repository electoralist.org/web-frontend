export class Suggestion {
    matchesInput(input: string): boolean {
        return false;
    }

    exactMatch(input: string): boolean {
        return false;
    }

    displayName(): string {
        return "";
    }
}

export class ZipSuggestion extends Suggestion {
    constructor(zipCode: string) {
        super();
        this.zipCode = zipCode;
    }

    matchesInput(input: string): boolean {
        return true;
    }

    displayName() {
        return this.zipCode;
    };
}


export class AddressSuggestion extends Suggestion {
    constructor(address: string, strength: number) {
        super();
        this.address = address;
        this.strength = strength;
    }

    matchesInput(input: string): boolean {
        return true;
    }

    displayName() {
        return this.address;
    };
}


export class StateSuggestion extends Suggestion {
    constructor(stateName: string, stateAbbreviation: string) {
        super();
        this.stateName = stateName;
        this.stateAbbreviation = stateAbbreviation;
    }

    matchesInput(input: string): boolean {
        const matches = this.stateName.toLowerCase().startsWith(input) || this.stateAbbreviation.toLowerCase() === input;
        console.log(`Does '${this.stateName.toLowerCase()}' match '${input}'? ${matches}`);
        return matches;
    }

    displayName() {
        return this.stateName;
    }
}

export class DistrictSuggestion extends Suggestion {
    constructor(districtCodes: string[], stateName: string, longDescription: string) {
        super();
        this.districtCodes = districtCodes;
        this.stateName = stateName;
        this.longDescription = longDescription;
    }

    matchesInput(input: string): boolean {
        return this.districtCodes.some(code => code.toLowerCase().startsWith(input)) || this.stateName.toLowerCase().startsWith(input);
    }

    displayName() {
        return `${this.districtCodes[0]} • ${this.longDescription}`;
    }
}

export class CurrentLocationSuggestion extends Suggestion {
    constructor() {
        super();
    }
    matchesInput(input: string): boolean {
        return true;
    }
    displayName() {
        return "Current Location";
    }
}
interface StateDescription {
    stateName: str;
    stateAbbreviation: str;
    districtCount: number;
}

const stateDescriptions: StateDescription[] = [
    {
        "stateName": "Alabama",
        "stateAbbreviation": "AL",
        "districtCount": 7
    },
    {
        "stateName": "Alaska",
        "stateAbbreviation": "AK",
        "districtCount": 1
    },
    {
        "stateName": "Arizona",
        "stateAbbreviation": "AZ",
        "districtCount": 9
    },
    {
        "stateName": "Arkansas",
        "stateAbbreviation": "AR",
        "districtCount": 4
    },
    {
        "stateName": "California",
        "stateAbbreviation": "CA",
        "districtCount": 52
    },
    {
        "stateName": "Colorado",
        "stateAbbreviation": "CO",
        "districtCount": 8
    },
    {
        "stateName": "Connecticut",
        "stateAbbreviation": "CT",
        "districtCount": 5
    },
    {
        "stateName": "Delaware",
        "stateAbbreviation": "DE",
        "districtCount": 1
    },
    {
        "stateName": "District of Columbia",
        "stateAbbreviation": "DC",
        "districtCount": 0
    },
    {
        "stateName": "Florida",
        "stateAbbreviation": "FL",
        "districtCount": 28
    },
    {
        "stateName": "Georgia",
        "stateAbbreviation": "GA",
        "districtCount": 14
    },
    {
        "stateName": "Hawaii",
        "stateAbbreviation": "HI",
        "districtCount": 2
    },
    {
        "stateName": "Idaho",
        "stateAbbreviation": "ID",
        "districtCount": 2
    },
    {
        "stateName": "Illinois",
        "stateAbbreviation": "IL",
        "districtCount": 17
    },
    {
        "stateName": "Indiana",
        "stateAbbreviation": "IN",
        "districtCount": 9
    },
    {
        "stateName": "Iowa",
        "stateAbbreviation": "IA",
        "districtCount": 4
    },
    {
        "stateName": "Kansas",
        "stateAbbreviation": "KS",
        "districtCount": 4
    },
    {
        "stateName": "Kentucky",
        "stateAbbreviation": "KY",
        "districtCount": 6
    },
    {
        "stateName": "Louisiana",
        "stateAbbreviation": "LA",
        "districtCount": 6
    },
    {
        "stateName": "Maine",
        "stateAbbreviation": "ME",
        "districtCount": 2
    },
    {
        "stateName": "Maryland",
        "stateAbbreviation": "MD",
        "districtCount": 8
    },
    {
        "stateName": "Massachusetts",
        "stateAbbreviation": "MA",
        "districtCount": 9
    },
    {
        "stateName": "Michigan",
        "stateAbbreviation": "MI",
        "districtCount": 13
    },
    {
        "stateName": "Minnesota",
        "stateAbbreviation": "MN",
        "districtCount": 8
    },
    {
        "stateName": "Mississippi",
        "stateAbbreviation": "MS",
        "districtCount": 4
    },
    {
        "stateName": "Missouri",
        "stateAbbreviation": "MO",
        "districtCount": 8
    },
    {
        "stateName": "Montana",
        "stateAbbreviation": "MT",
        "districtCount": 2
    },
    {
        "stateName": "Nebraska",
        "stateAbbreviation": "NE",
        "districtCount": 3
    },
    {
        "stateName": "Nevada",
        "stateAbbreviation": "NV",
        "districtCount": 4
    },
    {
        "stateName": "New Hampshire",
        "stateAbbreviation": "NH",
        "districtCount": 2
    },
    {
        "stateName": "New Jersey",
        "stateAbbreviation": "NJ",
        "districtCount": 12
    },
    {
        "stateName": "New Mexico",
        "stateAbbreviation": "NM",
        "districtCount": 3
    },
    {
        "stateName": "New York",
        "stateAbbreviation": "NY",
        "districtCount": 26
    },
    {
        "stateName": "North Carolina",
        "stateAbbreviation": "NC",
        "districtCount": 14
    },
    {
        "stateName": "North Dakota",
        "stateAbbreviation": "ND",
        "districtCount": 1
    },
    {
        "stateName": "Ohio",
        "stateAbbreviation": "OH",
        "districtCount": 15
    },
    {
        "stateName": "Oklahoma",
        "stateAbbreviation": "OK",
        "districtCount": 5
    },
    {
        "stateName": "Oregon",
        "stateAbbreviation": "OR",
        "districtCount": 6
    },
    {
        "stateName": "Pennsylvania",
        "stateAbbreviation": "PA",
        "districtCount": 17
    },
    {
        "stateName": "Rhode Island",
        "stateAbbreviation": "RI",
        "districtCount": 2
    },
    {
        "stateName": "South Carolina",
        "stateAbbreviation": "SC",
        "districtCount": 7
    },
    {
        "stateName": "South Dakota",
        "stateAbbreviation": "SD",
        "districtCount": 1
    },
    {
        "stateName": "Tennessee",
        "stateAbbreviation": "TN",
        "districtCount": 9
    },
    {
        "stateName": "Texas",
        "stateAbbreviation": "TX",
        "districtCount": 38
    },
    {
        "stateName": "Utah",
        "stateAbbreviation": "UT",
        "districtCount": 4
    },
    {
        "stateName": "Vermont",
        "stateAbbreviation": "VT",
        "districtCount": 1
    },
    {
        "stateName": "Virginia",
        "stateAbbreviation": "VA",
        "districtCount": 11
    },
    {
        "stateName": "Washington",
        "stateAbbreviation": "WA",
        "districtCount": 10
    },
    {
        "stateName": "West Virginia",
        "stateAbbreviation": "WV",
        "districtCount": 2
    },
    {
        "stateName": "Wisconsin",
        "stateAbbreviation": "WI",
        "districtCount": 8
    },
    {
        "stateName": "Wyoming",
        "stateAbbreviation": "WY",
        "districtCount": 1
    }
];


export const stateSuggestions: StateSuggestion[] = [];
export const districtSuggestions: DistrictSuggestion[] = [];

const ordinal: (number) => string = (cardinal: number) => {
    if (cardinal >= 10 && cardinal < 20) {
        return `${cardinal}th`;
    } else {
        if (`${cardinal}`.endsWith("1")) {
            return `${cardinal}st`;
        } else if (`${cardinal}`.endsWith("2")) {
            return `${cardinal}nd`;
        } else if (`${cardinal}`.endsWith("3")) {
            return `${cardinal}rd`;
        } else {
            return `${cardinal}th`;
        }
    }
};

for (let state of stateDescriptions) {
    stateSuggestions.push(new StateSuggestion(state.stateName, state.stateAbbreviation));
    if (state.districtCount === 1) {
        districtSuggestions.push(
            new DistrictSuggestion(
                [
                    `${state.stateAbbreviation}-AL`,
                    `${state.stateAbbreviation}-01`
                ],
                state.stateName,
                `${state.stateName}'s at-large district`)
        )
        ;
    } else {
        for (let districtNumber = 1; districtNumber <= state.districtCount; districtNumber++) {
            const paddedDistrictNumber = `${districtNumber}`.padStart(2, "0");
            districtSuggestions.push(
                new DistrictSuggestion(
                    [`${state.stateAbbreviation}-${paddedDistrictNumber}`],
                    state.stateName,
                    `${state.stateName}'s ${ordinal(districtNumber)} district`)
            );
        }
    }
}