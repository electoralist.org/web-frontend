import {useState, createRef} from 'react';
import { useNavigate } from "react-router-dom";

import 'react-bootstrap-typeahead/css/Typeahead.css';

import Spinner from 'react-bootstrap/Spinner'
import ProgressBar from 'react-bootstrap/ProgressBar';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import {Typeahead} from 'react-bootstrap-typeahead'
import {
    stateSuggestions,
    districtSuggestions,
    Suggestion,
    SuggestionGroup,
    StateSuggestion,
    DistrictSuggestion, CurrentLocationSuggestion, AddressSuggestion, ZipSuggestion
} from './searchBarSuggestions.tsx';
import {settings} from "../../settings/settings.tsx";


const currentLocationSuggestion = new CurrentLocationSuggestion();

function SearchBar() {


    const getSuggestionsForInput: (input: string) => Suggestion[] = (input) => {
        const lowerInput = input.toLowerCase();
        if (lowerInput.length > 1) {
            let inputDrivenSuggestions = [
                ...getStateSuggestionsForInput(lowerInput),
                ...getDistrictSuggestionsForInput(lowerInput),
                ...getZipSuggestionsForInput(lowerInput),
            ];
            if (!inputDrivenSuggestions.length && inputAddressSmellTest(input)) {
                inputDrivenSuggestions = getAddressSuggestionsForInput(input);
            }
            return [
                ...getCurrentLocationSuggestionsForInput(lowerInput),
                ...inputDrivenSuggestions
            ];
        } else return getCurrentLocationSuggestionsForInput(lowerInput);
    };

    const getStateSuggestionsForInput: (input: string) => StateSuggestion[] = (input) => {
        return stateSuggestions.filter(a => a.matchesInput(input));
    };

    const getDistrictSuggestionsForInput: (input: string) => DistrictSuggestion[] = (input) => {
        return districtSuggestions.filter(a => a.matchesInput(input));
    };

    const zipFormat = /\d{5}/;
    const zip9Format = /\d{5}-\d{4}/;

    const getZipSuggestionsForInput: (input: string) => ZipSuggestion[] = (input) => {
        if ((input.length == 5 && zipFormat.test(input))
            || (input.length == 10 && zip9Format.test(input))) {
            return [new ZipSuggestion(input)];
        } else {
            return [];
        }
    };

    const letterFormat = /[A-Za-z]/; // let's have at least one letter!

    const inputAddressSmellTest: (input: string) => boolean = (input) => {
        return input.length >= 3 && letterFormat.test(input);
    };

    const unitFormatStrengths = [
        /\d+ [A-Za-z0-9\. ]+/ , // A street address
        /\d+ [A-Za-z0-9\. ]+, [A-Za-z0-9\.\/ ]+/ , // Street plus city
        /\d+ [A-Za-z0-9\. ]+, [A-Za-z0-9\.\/ ]+, [A-Za-z0-9\. ]+, [A-Za-z]{2}/ , // Street plus city plus unit state
        /\d+ [A-Za-z0-9\. ]+, [A-Za-z0-9\.\/ ]+, [A-Za-z0-9\. ]+, [A-Za-z]{2} \d\d\d\d\d/ , // Street plus city plus unit state plus zip5
        /\d+ [A-Za-z0-9\. ]+, [A-Za-z0-9\.\/ ]+, [A-Za-z0-9\. ]+, [A-Za-z]{2} \d\d\d\d\d\-\d\d\d\d/ , // Street plus city plus unit state plus zip9
    ];
    const unitlessFormatStrengths = [
        /\d+ [A-Za-z0-9\. ]+/ , // A street address
        /\d+ [A-Za-z0-9\. ]+, [A-Za-z0-9\. ]+/ , // Street plus city
        /\d+ [A-Za-z0-9\. ]+, [A-Za-z0-9\. ]+, [A-Za-z]{2}/ , // Street plus city plus state
        /\d+ [A-Za-z0-9\. ]+, [A-Za-z0-9\. ]+, [A-Za-z]{2} \d\d\d\d\d/ , // Street plus city plus state plus zip5
        /\d+ [A-Za-z0-9\. ]+, [A-Za-z0-9\. ]+, [A-Za-z]{2} \d\d\d\d\d\-\d\d\d\d/ , // Street plus city plus state plus zip9
    ];

    const streetlessStrengths = [
        /[A-Za-z0-9\. ]+, [A-Za-z]{2}/ , // Street plus city plus state
        /[A-Za-z0-9\. ]+, [A-Za-z]{2} \d\d\d\d\d/ , // Street plus city plus state plus zip5
        /[A-Za-z0-9\. ]+, [A-Za-z]{2} \d\d\d\d\d\-\d\d\d\d/ , // Street plus city plus state plus zip9
    ];

    const getAddressSuggestionsForInput: (input: string) => AddressSuggestion[] = (input) => {
        if (!settings.addressSearchEnabled) {
            return []
        }
        else {
            const unitlessScore = unitlessFormatStrengths.filter(format => format.test(input)).length / unitlessFormatStrengths.length;
            const unitScore = unitFormatStrengths.filter(format => format.test(input)).length / unitFormatStrengths.length;
            const streetlessScore = streetlessStrengths.filter(format => format.test(input)).length / streetlessStrengths.length;
            return [new AddressSuggestion(input, Math.max(unitlessScore, unitScore, streetlessScore))];
        }
    }


    const getCurrentLocationSuggestionsForInput: (input: string) => CurrentLocationSuggestion[] = (input) => {
        if ((!input.length || "Current Location".toLowerCase().startsWith(input)) && geolocationAvailable) {
            return [currentLocationSuggestion];
        } else {
            return [];
        }
    };


    const [geolocationError, setGeolocationError] = useState<boolean>(false);

    const geolocationAvailable = navigator.geolocation && !geolocationError;

    const [gettingGeolocation, setGettingGeolocation] = useState<boolean>(false);
    const [options, setOptions] = useState<Suggestion[]>(geolocationAvailable ? [currentLocationSuggestion] : []);

    const typeaheadRef = createRef();
    const navigate = useNavigate();

    const noHint = options.length === 1 && options[0] instanceof AddressSuggestion;

    const geolocationSpinner = gettingGeolocation ? (
        <Spinner className="GeolocationSpinner" animation="border" role="status">
        <span className="VisuallyHidden">Getting geolocation...</span>
    </Spinner>) : null;

    return (<div>
            <p className="SmallSearchBarPrefix ShadowedText">Find federal races in or near</p>
            <InputGroup>
                <InputGroup.Text className="SearchBarPrefix">
                    Find federal races in or near
                </InputGroup.Text>
                <div className="SearchBarProgressWrapper">
                    {geolocationSpinner}
                <Typeahead
                    disabled={gettingGeolocation}
                    ref={typeaheadRef}
                    className={`SearchField ${noHint ? "NoHint" : ""}`}
                    id="search-form"
                    filterBy={() => true}
                    labelKey={(option: Suggestion) => option.displayName()}
                    onFocus={(event) => {
                        if (options.length == 1 && options[0] === currentLocationSuggestion) {
                            typeaheadRef.current.clear();
                        }
                    }}
                    onChange={(selected => {
                        if (selected[0] instanceof CurrentLocationSuggestion) {
                            typeaheadRef.current.clear();
                            setGettingGeolocation(true);
                            const geolocation = navigator.geolocation.getCurrentPosition((location) =>
                                {
                                    navigate(`/location-search?lat=${location.coords.latitude}&lon=${location.coords.longitude}`)
                                },
                                (error) => {
                                    setGettingGeolocation(false);
                                    setGeolocationError(true);
                                    setOptions(options.filter(x => x !== currentLocationSuggestion))
                                })
                            ;
                        }
                        else if (selected[0] instanceof DistrictSuggestion) {

                        }
                        else if (selected[0] instanceof StateSuggestion) {

                        }
                        else if (selected[0] instanceof ZipSuggestion) {

                        }
                        else if (selected[0] instanceof AddressSuggestion) {

                        }
                    })}
                    onInputChange={(text: string, event: Event) => {
                        const suggestions = getSuggestionsForInput(text);
                        console.log(`suggestions length: ${suggestions.length}`);
                        setOptions(suggestions);
                    }}
                    renderMenuItemChildren={(option: Object|String, props: Object, index: Number) => {
                        let itemBonus = null;
                        if (option instanceof CurrentLocationSuggestion) {
                            itemBonus = <div className="SearchSuggestionLabel">Use your device's geolocation</div>
                        }
                        else if (option instanceof ZipSuggestion) {
                            itemBonus = <div className="SearchSuggestionLabel">ZIP code</div>
                        }
                        else if (option.strength) {
                            let progressVariant = "danger";
                            if (option.strength >= 0.5) {
                                progressVariant = "warning"
                            }
                            if (option.strength >= 0.75) {
                                progressVariant = "success"
                            }
                            itemBonus = (<div className="StrengthMeterDiv">
                                <ProgressBar className="StrengthMeterBar" now={option.strength * 100} variant={progressVariant} />
                                <div className="StrengthMeterLabel">Address Quality</div>
                            </div>);
                        }
                        return (<div>
                            <div>{option.displayName()}</div>
                            {itemBonus}
                        </div>);
                    }}
                    options={options}
                    placeholder={gettingGeolocation ? "Geolocating..." : settings.addressSearchEnabled ? "State, district (e.g. CA-12), ZIP, or address" : "State, district (e.g. CA-12), ZIP"}
                    defaultSelected={geolocationAvailable ? options.slice(0, 1) : []}
                />
                </div>
                <Button
                    className="SearchBarButton"
                    onClick={null}
                    disabled={gettingGeolocation}
                >
                    Search
                </Button>
            </InputGroup>
        </div>
    );
}


export default SearchBar;