import { useState } from "react";
import { Link } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import {settings} from "../../settings/settings.tsx";

export enum NavBarPage {
    Home,
    QAndA
};

interface NavBarProps {
    className: string;
    currentPage: NavBarPage;
};

function NavBar(props: NavBarProps) {
    const [visible, setVisible] = useState(false);
    const links = [];
    if (!settings.maintenanceMode && props.currentPage !== NavBarPage.Home) {
    links.push(<li key="home">
                       <Link to="/">Search</Link>
                       </li>);
    }
    else if (!settings.maintenanceMode) {
        links.push(<li  key="home">
            Search
        </li>);
    }
    if (!settings.maintenanceMode && props.currentPage !== NavBarPage.QAndA) {
        links.push(<li  key="q-and-a">
                               <Link to="/q-and-a">Q&A</Link>
                          </li>);
        }
    else if (!settings.maintenanceMode) {
        links.push(<li key="q-and-a">
                    Q&A
                </li>);
        }
        links.push(<li key="tip-jar">
            <a href="http://SNJ TODO">Tip jar</a>
        </li>);

  return (<nav className={props.className}>
    <Button onClick={() => {setVisible(!visible);}} className={visible ? "open" : "closed"} id="menuToggle">☰</Button>
    <ul className={visible ? "openMobileNav" : "closedMobileNav"}>
     {links}

     </ul>
  </nav>
  );
}

export default NavBar;