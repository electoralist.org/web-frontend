import ChromeWrapper from '../ChromeWrapper.tsx';
import Maintenance from "../pages/maintenance/Maintenance.tsx";

function MaintenanceModeRoute({match, location}) {
    document.title = "electoralist.org is down for maintenance";
    console.log(location);
    return <ChromeWrapper
        wrappedComponent={Maintenance()}
        currentPage={null}
    />;
}

export default MaintenanceModeRoute;
