import ChromeWrapper from '../ChromeWrapper.tsx';
import Candidate from '../pages/party/Party.tsx';

function PartyRoute({match, location}) {
    document.title = "electoralist.org Party";
    console.log(location);
    return <ChromeWrapper
        wrappedComponent={Party()}
        currentPage={null}
    />;
}

export default PartyRoute;
