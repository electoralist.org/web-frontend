import ChromeWrapper from '../ChromeWrapper.tsx';
import AddressSearch from '../pages/addressSearch/AddressSearch.tsx';

function AddressSearchRoute({match, location}) {
    document.title = "electoralist.org Address Search";
    console.log(location);
    return <ChromeWrapper
        wrappedComponent={AddressSearch()}
        currentPage={null}
    />;
}

export default AddressSearchRoute;
