import ChromeWrapper from '../ChromeWrapper.tsx';
import MainPage from '../pages/index/MainPage.tsx';
import {NavBarPage} from '../components/nav/NavBar.tsx';

function MainPageRoute() {
  return <ChromeWrapper
  wrappedComponent={MainPage()}
  currentPage={NavBarPage.Home}
  />;
}

export default MainPageRoute;
