import ChromeWrapper from '../ChromeWrapper.tsx';
import StateSearch from '../pages/stateSearch/StateSearch.tsx';

function StateSearchRoute({match, location}) {
    document.title = "electoralist.org State Search";
    console.log(location);
    return <ChromeWrapper
        wrappedComponent={StateSearch()}
        currentPage={null}
    />;
}

export default StateSearchRoute;
