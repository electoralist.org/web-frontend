import ChromeWrapper from '../ChromeWrapper.tsx';
import DistrictSearch from '../pages/districtSearch/DistrictSearch.tsx';

function DistrictSearchRoute({match, location}) {
    document.title = "electoralist.org District Search";
    console.log(location);
    return <ChromeWrapper
        wrappedComponent={DistrictSearch()}
        currentPage={null}
    />;
}

export default DistrictSearchRoute;
