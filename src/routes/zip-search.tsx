import ChromeWrapper from '../ChromeWrapper.tsx';
import ZipSearch from '../pages/zipSearch/ZipSearch.tsx';

function ZipSearchRoute({match, location}) {
    document.title = "electoralist.org ZIP Search";
    console.log(location);
    return <ChromeWrapper
        wrappedComponent={ZipSearch()}
        currentPage={null}
    />;
}

export default ZipSearchRoute;
