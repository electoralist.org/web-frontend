import ChromeWrapper from '../../ChromeWrapper.tsx';
import HouseRace from '../../pages/houseRace/HouseRace.tsx';

function HouseRaceRoute({match, location}) {
    document.title = "electoralist.org House Race";
    console.log(location);
    return <ChromeWrapper
        wrappedComponent={HouseRace()}
        currentPage={null}
    />;
}

export default HouseRaceRoute;
