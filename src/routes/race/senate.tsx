import ChromeWrapper from '../../ChromeWrapper.tsx';
import SenateRace from '../../pages/senateRace/SenateRace.tsx';

function SenateRaceRoute({match, location}) {
    document.title = "electoralist.org Senate Race";
    console.log(location);
    return <ChromeWrapper
        wrappedComponent={SenateRace()}
        currentPage={null}
    />;
}

export default SenateRaceRoute;
