import ChromeWrapper from '../../ChromeWrapper.tsx';
import PresidentRace from '../../pages/presidentRace/PresidentRace.tsx';

function PresidentRaceRoute({match, location}) {
    document.title = "electoralist.org President Race";
    console.log(location);
    return <ChromeWrapper
        wrappedComponent={PresidentRace()}
        currentPage={null}
    />;
}

export default PresidentRaceRoute;
