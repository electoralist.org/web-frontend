import ChromeWrapper from '../ChromeWrapper.tsx';
import Faq from '../pages/faq/Faq.tsx';
import {NavBarPage} from '../components/nav/NavBar.tsx';

function QandARoute() {
  document.title = "electoralist.org Questions and Answers"
  return <ChromeWrapper
  wrappedComponent={Faq()}
  currentPage={NavBarPage.QAndA}
  />;
}

export default QandARoute;
