import ChromeWrapper from '../ChromeWrapper.tsx';
import SubmitCandidate from '../pages/submitCandidate/SubmitCandidate.tsx';

export default function SubmitCandidateRoute() {
  document.title = "Submit a candidate to electoralist.org"
  return <ChromeWrapper
  wrappedComponent={SubmitCandidate()}
  currentPage={null}
  />;
}