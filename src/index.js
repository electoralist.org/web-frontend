import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

import './bootstrap.min.css';
import './App.css';

import { BrowserRouter,
 Routes,
 Route
} from "react-router-dom"

import SubmitCandidateRoute from './routes/submit-candidate.tsx'
import MainPageRoute from './routes/index.tsx'
import FaqRoute from './routes/q-and-a.tsx'
import LocationSearchRoute from "./routes/location-search.tsx";
import ZipSearchRoute from "./routes/zip-search.tsx";
import AddressSearchRoute from "./routes/address-search.tsx";
import StateSearchRoute from "./routes/state-search.tsx";
import DistrictSearchRoute from "./routes/district-search.tsx";
import PresidentRaceRoute from "./routes/race/president.tsx";
import SenateRaceRoute from "./routes/race/senate.tsx";
import HouseRaceRoute from "./routes/race/house.tsx";
import CandidateRoute from "./routes/candidate.tsx";
import PartyRoute from "./routes/party.tsx";
import MaintenanceModeRoute from "./routes/maintenance-mode.tsx";
import {settings} from "./settings/settings.tsx";

const maintenanceRoute = settings.maintenanceMode ? (<MaintenanceModeRoute />) : null;


ReactDOM.render(
  <React.StrictMode>
  <BrowserRouter>
  <Routes>
      <Route path="/" element={maintenanceRoute || <MainPageRoute />} />
      <Route path="/submit-candidate" element={maintenanceRoute || <SubmitCandidateRoute />} />
      <Route path="/q-and-a" element={maintenanceRoute || <FaqRoute />} />
      <Route path="/location-search" element={maintenanceRoute || <LocationSearchRoute />} />
      <Route path="/zip-search" element={maintenanceRoute || <ZipSearchRoute />} />
      <Route path="/address-search" element={maintenanceRoute || <AddressSearchRoute />} />
      <Route path="/state-search" element={maintenanceRoute || <StateSearchRoute />} />
      <Route path="/district-search" element={maintenanceRoute || <DistrictSearchRoute />} />
      <Route path="/race/senate/:stateCode" element={maintenanceRoute || <SenateRaceRoute />} />
      <Route path="/race/house/:districtCode" element={maintenanceRoute || <HouseRaceRoute />} />
      <Route path="/race/president/:stateCode" element={maintenanceRoute || <PresidentRaceRoute />} />
      <Route path="/candidate/:candidateId" element={maintenanceRoute || <CandidateRoute />} />
      <Route path="/party/:partyId" element={maintenanceRoute || <PartyRoute />} />
  </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
