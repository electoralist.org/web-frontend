import {locationSearch as locationSearchApi, LocationSearchResults} from "../apis/LocationSearchAPI.tsx";
import {APIError} from "../apis/Error.tsx";


export const locationSearch: (lat: string, lon: string) => Promise<LocationSearchResults | APIError> = async (lat, lon) => {
    return await locationSearchApi(lat, lon);
};