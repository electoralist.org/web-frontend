import SearchBar from '../../components/search/SearchBar.tsx';
import NavBar, {NavBarPage} from '../../components/nav/NavBar.tsx';
import Form from 'react-bootstrap/Form';

function MainPage() {
  return (
          <div className="MainPage">

        <h1 className="HomeHeader ShadowedText">electoralist.org</h1>
      <Form className="SearchForm">
         <Form.Group>
            <SearchBar />
           </Form.Group>
         </Form>
         <p className="ShadowedText">electoralist.org helps you find and support socialist candidates running near you.</p>
         </div>
  );
}

export default MainPage;
