import NonHomePage from '../../components/nonHomePage/NonHomePage.tsx'
import { Link } from "react-router-dom";

function Faq() {
  return (NonHomePage(<div>
          <h1>Questions and answers</h1>

<dl>
   <dt>Why did you create this site?</dt>
   <dd>I wanted to give socialists who are interested in electoralism an easy way to
   identify where candidates are running, learn what it will take to get candidates on the ballot,
   and decide what candidates are worthy of their support.
   </dd>

   <dt>Why isn't my candidate listed?</dt>
   <dd>Our information is crowd-sourced. You may need to <Link to="/submit-candidate">submit the candidate's information.</Link></dd>

   <dt>Why isn't my submitted candidate listed?</dt>
   <dd>When I receive a submission, I check that the candidate meets the standards before listing them.
   The candidate may be in review.</dd>

   <dt>What standards must a candidate meet to be listed?</dt>
   <dd>To be listed, a candidate must be a socialist or communist. Candidates can run independently or as
   a candidate for a socialist or communist party. Candidates cannot be running as a candidate for a
   non-socialist or communist party, such as the Democratic Party, the Republican Party, the Green Party,
   the Working Families Party, etc. In races where no socialist candidate obtained ballot access, write-in
   candidates running organized write-in campaigns can be listed.</dd>

   <dt>Only federal races?</dt>
   <dd>For now.</dd>

   <dt>What is electoralism?</dt>
   <dd>Electoralists seek to use bourgeois elections in their pursuit of socialism. They may seek to win a
   parliamentary majority and establish socialism by changing laws, or to win seats to inform people about socialism
   and call for its establishment by other means.</dd>

   <dt>Who are you?</dt>
   <dd>I'm just a communist making this site on a voluntary basis in my free time.</dd>

   <dt>How can I contact you?</dt>
   <dd>You can email <a href="mailto:steven@electoralist.org">steven@electoralist.org</a>.</dd>

   <dt>Do you belong to any party?</dt>
   <dd>No.</dd>

   <dt>Who pays for the site?</dt>
   <dd>I do. You can <a href="http://google.com/">pitch in</a> to keep it running.</dd>

   <dt>How else can I contribute?</dt>
   <dd><a href="https://gitlab.com/electoralist.org">The site is open-source, so you can contribute to its code, data and
   design.</a> You can also spread the word! I also encourage you to get involved with races near you and to participate
      in non-electoralist projects.</dd>
</dl>
</div>));
}

export default Faq;