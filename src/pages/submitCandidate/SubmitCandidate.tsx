import NonHomePage from "../../components/nonHomePage/NonHomePage.tsx";

function SubmitCandidate() {
    return new NonHomePage(
        (<div>
            <h1>Submit Candidate</h1>
        </div>)
    );
}

export default SubmitCandidate;