import NonHomePage from "../../components/nonHomePage/NonHomePage.tsx";

function Maintenance() {
    return new NonHomePage(
        (<div>
            <h1>electoralist.org is down for maintenance</h1>
        </div>)
    );
}

export default Maintenance;