import {useState} from 'react';
import useAsyncEffect from 'use-async-effect';

import NonHomePage from "../../components/nonHomePage/NonHomePage.tsx";
import {useSearchParams} from "react-router-dom";
import {locationSearch} from "../../services/LocationSearchService.tsx";
import {LocationSearchResults} from "../../apis/LocationSearchAPI";

function LocationSearch() {
    const [searchParams, setSearchParams] = useSearchParams();
    const lat = searchParams.get("lat");
    const lon = searchParams.get("lon");
    const [searchResults, setSearchResults] = useState<LocationSearchResults>();
    useAsyncEffect(async () => {
        const locations = await locationSearch(lat, lon);
        setSearchResults(locations);
    }, [lat, lon]);

    console.log(searchResults);
    return new NonHomePage(
        (<div>
            <h1>Location Search</h1>
        </div>)
    );
}

export default LocationSearch;