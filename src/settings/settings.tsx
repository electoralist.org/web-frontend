
export interface Settings {
   locationsBackend: string,
   statesBackend: string,
   districtsBackend: string,
   candidatesBackend: string,
   partiesBackend: string,
   submissionsBackend: string,
   addressSearchEnabled: boolean,
   maintenanceMode: boolean,
}

const prodSettings: Settings = {
   locationsBackend: "/api/locations/search/",
   statesBackend: "/api/locations/states/",
   districtsBackend: "/api/locations/districts/",
   candidatesBackend: "/api/candidates/",
   partiesBackend: "/api/parties/",
   submissionsBackend: "/api/submissions/",
   addressSearchEnabled: true,
   maintenanceMode: false,
}

const devSettings: Settings = {
   locationsBackend: "/api/locations/search/",
   statesBackend: "/api/locations/states/",
   districtsBackend: "/api/locations/districts/",
   candidatesBackend: "/api/candidates/",
   partiesBackend: "/api/parties/",
   submissionsBackend: "/api/submissions/",
   addressSearchEnabled: true,
   maintenanceMode: false
}

export const settings: Settings = process.env.NODE_ENV === 'development' ? devSettings : prodSettings;