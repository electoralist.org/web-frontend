import Component from 'react';
import './App.css';
import './bootstrap.min.css';
import NavBar, {NavBarPage} from './components/nav/NavBar.tsx'
import React from "react";

interface ChromeWrapperProps {
    wrappedComponent: Component;
    currentPage: NavBarPage;
}

function ChromeWrapper(props: ChromeWrapperProps) {
let mobileNav = <NavBar className="MobileNav" currentPage={props.currentPage} />;
let topNavBar = <NavBar className="TopNav" currentPage={props.currentPage} />;
let bottomNavBar = <NavBar className="BottomNav" currentPage={props.currentPage} />;

  return (
    <div className="App">
        <div className="DrawableArea">
            {mobileNav}
            {topNavBar}
            {props.wrappedComponent}
            {bottomNavBar}
        </div>
    </div>
  );
}

export default ChromeWrapper;
