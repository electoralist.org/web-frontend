import { APIError } from './Error.tsx';
import { settings } from '../settings/settings.tsx'
// pointless comment
export interface BallotAccess {
    filingDeadline: string,
    filingFee: number,
    signatureRequirement: number,
    applicablePartyIds: number[],
    appliesToUnlistedParties: boolean,
}

export interface WriteInAccess {
    filingDeadline: string,
    filingFee: number,
    signatureRequirement: number,
}

export interface PriorResults {
    eligibleVoters: number,
    registeredVoters: number,
    partyResults: PartyResult[],
}

export interface PartyResult {
    partyId: number,
    partyName: string,
    votes: number;
}

export interface LocationCandidateSummary {
    candidateName: string,
    candidateId: number,
    partyName: string
    partyId: number,
}

export interface LocationSearchResult {
    resultType:  "DISTRICT_RACE" | "SENATE_RACE" | "PRESIDENT_RACE",
    electionCycleYear: number,
    electionDate: string,
    comments: string,
    extendedComments: string,
    resultName: string,
    candidatesWithAccess: LocationCandidateSummary[],
    candidatesSeekingAccess: LocationCandidateSummary[],
    ballotAccesses: BallotAccess,
    writeInAccess: WriteInAccess,
    priorResults: PriorResults
}

export interface LocationSearchResults {
    results: LocationSearchResult[],
}

export const locationSearch: (lat: string, lon: string) => Promise<LocationSearchResults | APIError> = async (lat, lon) => {
    const searchUrl = `${settings.locationsBackend}v1?lat=${lat}&lon=${lon}`;
    return fetch(searchUrl);
};