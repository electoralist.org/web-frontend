
export interface APIError {
    errorCode: number;
    message: String;
}