import {APIError} from "./Error";

export interface PartySummary {
    name: string,
    website: string,
}

export interface PoliticalHorizon {
    horizon: string,
    stagist: boolean,
    classless: boolean,
    moneyless: boolean,
    stateless: boolean,
    freeAssociation: boolean,
}

export interface ProgramDetails {
    hasProgram: boolean,
    description: string,
    workweek: WorkweekProgram,
    healthcare: HealthcareProgram,
    housing: HousingProgram,
}

export interface WorkweekProgram {
    hasWorkweekReductionProgram: boolean,
    workweekHours: number,
    annualVacationDays: number,
    annualVacationWeeks: number,
    parentalLeave: boolean,
}

export interface HealthcareProgram {
    hasUniversalHealthcareProgram: boolean,
    disabledFullParticipation: boolean,
    description: string,
}

export interface HousingProgram {
    hasUniversalHousingProgram: boolean,
    universalHousingProgramDescription: string,
    hasSafeHousingProgram: boolean,
    safeHousingProgramDescription: boolean,
}

export interface FoodProgram {
    hasFoodProgram: boolean,
    description: string,
}

export interface LaborProgram {
    hasLaborProgram: boolean,
    alienation: boolean,

    immigrantSolidarity: boolean,
    description: string
}

export interface EducationProgram {
    hasEducationProgram: boolean,
    abolishManualIntellectualDistinction: boolean,
    description: string,
}


export interface DemocratizationProgram {
    hasProgram: boolean,
    representativeDemocracy: boolean,
    directDemocracy: boolean,
    sortition: boolean,
    councilist: boolean,
    universalAdultSuffrage: boolean,
}

export interface EconomicProgram {
    reorganizeEconomy: boolean,
    parecon: boolean,
    planned: boolean,
    democraticPlanning: boolean,
    laborTimeAccounting: boolean,
    markets: boolean,
    marketSocialism: boolean,
    ecologicalProduction: boolean,
    ecologicalConsumption: boolean,
    abolishPrivateProperty: boolean,
    concreteAbolitionProgram: boolean,
    description: string
}


export interface JusticeProgram {
    hasJusticeProgram: string,
    description: string,
}

export interface InternationalismDetails {
    internationalist: boolean,
    description: string
}

export interface ImperialismDetails {
    antiImperialist: boolean,
    description: string,
    nonInterventionist: boolean,
    nonInterventionistDescription: string,
    withdrawalFromMilitaryAlliances: boolean,
    withdrawalFromIMFWB: boolean,
    disarmament: boolean,
    antiInternalImperialism: boolean,
    indigenousSovereignty: boolean,
    indigenousLandProgram: boolean,
    erradicateRacism: boolean,
    eliminateRacialDisparities: boolean,
}

export interface EcologyDetails {
    ecologicalPerspective: boolean,
    description: string,
}

export interface WomenGenderAndSexualityDetails {
    fullEquality: boolean,
    socialReproductionPerspective: boolean,
    reproductiveRights: boolean,
    description: string,
}

export interface ElectoralismDetails {
    neoKautskian: boolean,
    abstentionist: boolean,
    collaboratesWithBourgeoisParties: boolean,
    collaboratesWithOtherSocialistParties: boolean,
}

export interface PartyOrganizationDetails {
    democratic: boolean,
    democraticCentralist: boolean,
}

export interface LastCenturyDetails {
    description: string
}

export interface ActuallyExistingSocialism {
    description: string
}



export interface RelatedParty {
    description: string,
    partyId: number
}

export interface PartyDetail {
    name: string,
    description: string,
    politicalHorizon: PoliticalHorizon,
    internationalism: InternationalismDetails,
    imperialism: ImperialismDetails,
    electoralism: ElectoralismDetails,
    programatism: ProgramDetails,
    partyOrganization: PartyOrganizationDetails,
    foodProgram: FoodProgram,
    laborProgram: LaborProgram,
    educationProgram: EducationProgram,
    governanceProgram: DemocratizationProgram,
    economicProgram: EconomicProgram,
    justiceProgram: JusticeProgram,
    ecology: EcologyDetails,
    gender: WomenGenderAndSexualityDetails,
    lastCentury: LastCenturyDetails,
    existing: ActuallyExistingSocialism,
    relatedParties: RelatedParty[]

}

export const getPartySummary: (partyId: number) => PartySummary | APIError = (partyId) => {
    return null;
};

export const getPartySummaries: (partyIds: number[]) => PartySummary[] | APIError = (partyId) => {
    return null;
};

export const getPartyDetail: (partyId: number) => PartyDetail | APIError = (partyId) => {
    return null;
};